﻿using UnityEngine;

public class AnimationCoinFrame : MonoBehaviour
{
    public GameObject[] frames;

    private void OnEnable()
    {
        foreach(var frame in frames)
            frame.SetActive(false);
        
    }

    void Update()
    {

        if (Player.score >= 50 && Player.score < 75)
            frames[0].SetActive(true);
        if (Player.score >= 75 && Player.score < 100)
            frames[1].SetActive(true);
        if (Player.score >= 100 && Player.score < 125)
            frames[2].SetActive(true);
        if (Player.score >= 125) 
            frames[3].SetActive(true);
    }
}
